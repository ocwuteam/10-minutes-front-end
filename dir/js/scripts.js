//@prepros-append modules/init__slider.js
//@prepros-append modules/ios_scroll.js
//@prepros-append modules/main.js
var maxWidth = 630;

$(function(){


	// slick init
	$('.slider-js').slick({
		rtl: true,
		arrows: true,
		slidesToShow: 4,
		infinite: true,
		responsive: [
			{
				breakpoint: 998,
				settings: {
					slidesToShow: 2,
					spaceBetween: 0,
					dots: false
				}
			},
			{
				breakpoint: maxWidth,
				settings: 'unslick'
			}
		]
	});
	$('.guides-slider-js').slick({
		rtl: true,
		arrows: true,
		dots: false,
		slidesToShow: 3,
		infinite: true,
		responsive: [
			{
				breakpoint: 998,
				settings: {
					slidesToShow: 2,
					spaceBetween: 0,
					dots: false
				}
			},
			{
				breakpoint: maxWidth,
				settings: 'unslick'
			}
		]
	});

	$('.js-aside-slider').slick({
		rtl: true,
		arrows: true,
		dots: false,
		slidesToShow: 1,
		infinite: true
	});

	var bannerSlider = $('.js-banner-slider').slick({
		rtl: true,
		arrows: false,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		slidesPerRow: 1,
		rows: 0,
		infinite: true,
		dotsClass: "banner-slider__pagination"
	});

	$('.gallery_slider-js').slick({
		rtl: true,
		arrows: false,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		slidesPerRow: 1,
		rows: 0,
		infinite: true,
		dotsClass: "banner-slider__pagination"
	});


})
$(window).on('resize orientationChange', function(){
	var width = $(window).width();
	if(width > maxWidth) {
		$('.slider-js').slick({
			rtl: true,
			arrows: true,
			slidesToShow: 4,
			infinite: true,
			responsive: [
				{
					breakpoint: 998,
					settings: {
						slidesToShow: 2,
						spaceBetween: 0,
						dots: false
					}
				},
				{
					breakpoint: maxWidth,
					settings: 'unslick'
				}
			]
		});
		$('.guides-slider-js').slick({
			rtl: true,
			arrows: true,
			dots: false,
			slidesToShow: 3,
			infinite: true,
			responsive: [
				{
					breakpoint: 998,
					settings: {
						slidesToShow: 2,
						spaceBetween: 0,
						dots: false
					}
				},
				{
					breakpoint: maxWidth,
					settings: 'unslick'
				}
			]
		});
	}
});
var mob = 992; //width for hidding top nav
var lastWindowWidth = window.innerWidth;
var actual_scroll; //current scroll from top position
var is_ready = false; //is page loaded

window.addEventListener("touchmove", Scroll);
window.addEventListener("scroll", Scroll);

/* Define is device touchable */
function is_touch_device() {
	return 'ontouchstart' in window || navigator.maxTouchPoints
}


/* Define is device on iOS */
function is_iOS() {
	var iDevices = [
		'iPad Simulator',
		'iPhone Simulator',
		'iPod Simulator',
		'iPad',
		'iPhone',
		'iPod'
	];
	while (iDevices.length) {
		if (navigator.platform === iDevices.pop()) {
			return true
		}
	}
	return false
}


window.onload = function () {
	is_ready = true;
	Scroll()
}


$(document).ready(function () {
	if (is_iOS() && is_touch_device() && window.innerWidth < 1000) {
		$("html").addClass("nm-ios");
		$(".nm-main-overlay").css("cursor", "pointer")
	}
})

function Scroll() {
	var scrollPos = $(document).scrollTop();
}

//menu toggle
$(".header__expand--open").on("click", function (e) {
	e.preventDefault();

	actual_scroll = $(window).scrollTop();
	$("body,html").addClass("fixed");
	if (window.innerWidth <= mob) {
		$('body').addClass('header__active');
		$("body").scrollTop(actual_scroll)
	}
})


//aside close button
$(".header__expand--close").on("click", function (e) {
	e.preventDefault();

	$("body,html").removeClass("fixed");
	if (window.innerWidth <= mob) {
		$('body').removeClass('header__active');
		$(window).scrollTop(actual_scroll)
	}
})
$(function() {
	imageFill();
	$('.tabs-js').tabs();

	if(window.innerWidth >= 992){
		$('.header__ul').on('mouseover', 'li', function(){
		    $(this).addClass('open');
		});
		$('.header__ul').on('mouseleave', 'li', function(){
		    $(this).removeClass('open');
		});
	}

	if(window.innerWidth <= 992){
		/*
		$('.header__item').on('click', function (event) {
			event.stopPropagation();
			var $this = $(this);
			var $thisli = $thisli.siblings('li');
			console.log("click");
			if(!($this.hasClass('open'))){	
				$('.header__item').removeClass('open');
				$('.header__item').find('.dropdown__wrap').slideUp();
				$this.addClass('open');
				$this.find('.dropdown__wrap').slideDown();
				console.log('haven"t class');
			} else{
				$('.header__item').removeClass('open');
				$('.header__item').find('.dropdown__wrap').slideUp();
				console.log('have class');
			}
		});*/

		$('.header__link').on('click', function (event) {
			var $thisli = $(this).closest('li');
			if(!($thisli.hasClass('open'))){
				$thisli.siblings('li').find('.dropdown__wrap').slideUp();
				$thisli.removeClass('open');

				$thisli.addClass('open');
				$thisli.find('.dropdown__wrap').slideDown();
			} else{
				$thisli.removeClass('open');
				$thisli.find('.dropdown__wrap').slideUp();
			}
		});

		$('.dropdown__menu-title').on('click', function (event) {
			var $this = $(this);
			if(!($this.hasClass('open'))){
				$('.dropdown__menu-title').next('.dropdown__menu-list').slideUp();
				$('.dropdown__menu-title').removeClass('open');

				$this.addClass('open');
				$this.next('.dropdown__menu-list').slideDown();
			} else{
				$('.dropdown__menu-title').removeClass('open');
				$('.dropdown__menu-title').next('.dropdown__menu-list').slideUp();
			}
		});
	}


	$('.js-add-comment').on('click', function(){
		$(this).addClass('active');
		$('.js-new-comment').show('slow')
	});


	/**
	 *	Timer Blocks
	 */
	$(".js-timer-btn").each(function() {

		var $timerBtn = $(this);
		var $timerContainer = $timerBtn.closest(".js-timer");
		var $timerBody = $(".js-timer-body", $timerContainer);
		var $timerCounterWrapper = $(".js-timer-counter-wrapper", $timerContainer);
		var $timerCounter = $(".js-timer-counter", $timerContainer);
		var $timerTimelineBar = $(".js-timer-timeline-bar", $timerContainer);

		var counterTime = {
			hours: $timerCounter.data('hours') || 0,
			minutes: $timerCounter.data('minutes') || 0,
			seconds: $timerCounter.data('seconds') || 0
		};
		
		var $timerCloseBtn = $(".js-timer-close-btn", $timerContainer);
		var $timerPauseBtn = $(".js-timer-pause-btn", $timerContainer);
		var $timerMuteBtn = $(".js-timer-mute-btn", $timerContainer);
		var duration = ( counterTime.hours * 60 * 60 * 1000 ) + ( counterTime.minutes * 60 * 1000 ) + ( counterTime.seconds * 1000 );

		var finalTime;
		var pauseTime = 0;
		var progress = 0;

		var objSound;
		var muted = false;

		$timerBtn.on("click", function() {

			var currentTime = new Date().getTime();

			finalTime = currentTime + duration;
			var step = 100 / ( duration / 1000 );

			$timerTimelineBar.css({"width" : 0});
			progress = 0;

			$timerBtn.hide();
			$timerBody.show();

			$timerPauseBtn.removeClass("paused");
			$timerContainer.removeClass("paused");

			$timerCounter
			.countdown(finalTime, function(event) {
				$timerCounter.html( event.strftime('%H:%M:%S') );
			})
			.on('update.countdown', function() {
				if (progress < 100) {
					if (100 - progress <= step) {
						progress = 100
					} else {
						progress += step;
					}

					$timerTimelineBar.css({"width" : progress + "%"});
				}
			})
			.on('finish.countdown', function() {
				if (progress < 100) {
					progress = 100;
				}
				$timerTimelineBar.css({"width" : progress + "%"});


				var root = window.location.protocol + "//" + window.location.host + "/";

				objSound = document.createElement("audio");
				objSound.src = root + "dir/media/timer.mp3";
				objSound.volume = muted ? 0 : 1;
				objSound.autoPlay = false;
				objSound.preLoad = true;
				objSound.controls = true;
				objSound.play();
			});
		});

		$timerCloseBtn.on("click", function() {

			$timerBtn.show();
			$timerBody.hide();
			
			objSound.pause();
			delete objSound.src;

			$timerMuteBtn.removeClass("active");
			muted = false;

			$timerCounter.remove();
			$timerCounterWrapper
			.append('<div class="timer__counter js-timer-counter" data-hours="' + counterTime.hours + '" data-minutes="' + counterTime.minutes + '" data-seconds="' + counterTime.seconds + '"></div>');
			$timerCounter = $(".js-timer-counter", $timerContainer);
		});

		$timerMuteBtn.on("click", function() {
			if (muted == false) {
				$timerMuteBtn.addClass("active");
				muted = true;
				objSound.volume = 0;
			} else {
				$timerMuteBtn.removeClass("active");
				muted = false;
				objSound.volume = 1;
			}
		});

		$timerPauseBtn.on("click", function() {

			var $this = $(this);
			var $timerContainer = $this.closest(".js-timer");
			var $timerBody = $(".js-timer-body", $timerContainer);

			if ( $this.hasClass("paused") ) {

				var offsetPauseTime = new Date().getTime() - pauseTime.getTime();
				finalTime = finalTime + offsetPauseTime;
				$timerCounter
				.countdown(finalTime, function(event) {
					$timerCounter.html( event.strftime('%H:%M:%S') );
				})

				$this.removeClass("paused");
				$timerContainer.removeClass("paused");

			} else {

				pauseTime = new Date();
				$timerCounter.countdown("pause");
				$this.addClass("paused");
				$timerContainer.addClass("paused");

			}
		});
	});


	$('.select-js').SumoSelect();

	$('.js-filters').on('click', function(){
		$(this).toggleClass('active');
		$(this).next('.filters').slideToggle();
	});


	if(window.innerWidth <= 992){
		$('.footer__col').on('click', '.footer__title', function(){
			$(this).toggleClass('active');
			$(this).next('.footer__ul').slideToggle();
			$(this).next('.footer__text').slideToggle();
		})
	}

});

// fill images function
function imageFill() {
	if ($(".js-img").length) {
		var img_count = $(".js-img").length;
		for (var i = 0; i < img_count; i++) {
			var jthis = $(".js-img").eq(i);
			var jimg = jthis.children("img");
			jimg.removeAttr("style");
			if (jimg.outerWidth() < jthis.outerWidth()) {
				jimg.css("width", "100%").css("height", "auto");
			} else {
				if (jimg.outerHeight() < jthis.outerHeight()) {
					jimg.css("width", "auto").css("height", "100%");
				}
			}
		}
	}
}
$(window).resize(function() {
	imageFill();
});

// Sticky navbar

$(window).on('scroll', function(){
	if(window.innerWidth >= 992){
		var $header = $('.header'),
			pos = $('.header__top').outerHeight(),
			pad = $('.header__main-wrap').outerHeight();
		
		if ( $(window).scrollTop() >= pos ) {
			$header.addClass('fixed');
			$header.css("padding-bottom", pad);
		} else {
			$header.removeClass('fixed');
			$header.css("padding-bottom", "");
		}

	}

})


/**
 * Search Autocomplete
 */

$( function() {

	$.widget( "custom.catcomplete", $.ui.autocomplete, {

		close: function( event ) {
			this.cancelSearch = true;
			this._close( event );
		},

		_close: function( event ) {

			// Remove the handler that closes the menu on outside clicks
			this._off( this.document, "mousedown" );

			if ( this.menu.element.is( ":visible" ) ) {
				this.menu.element.hide();
				this.menu.blur();
				this.isNewMenu = true;
				this._trigger( "close", event );

				$(".js-header-search-results").removeClass("active");
			}
		},
		
		_create: function() {
			this._super();
			this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
		},
		
		_renderMenu: function( ul, items ) {
			var that = this,
			currentCategory = "";
			$.each( items, function( index, item ) {
				var li, submenuUL;
				if ( item.category != currentCategory ) {
					var elemCategory = $("<li class='ui-autocomplete-category'>" + item.category + "</li>");
					var subMenu = $("<div class='submenu " + item.category + "'><ul></ul></div>");

					elemCategory.append(subMenu);
					ul.append(elemCategory);
					currentCategory = item.category;
				}
				submenuUL = ul.find("." + item.category + " ul");
				li = that._renderItemData( submenuUL, item );
				if ( item.category ) {
					li.attr( "aria-label", item.category + " : " + item.label ).attr("cat-img", item.imgSrc).addClass("ui-menu-item js-autocomplete-item");
				}
			});

			$(".js-header-search-results").addClass("active");
		}
	});



	var data = [
		{ label: "anders", category: "מתכונים", imgSrc: "img/cat-0.png" },
		{ label: "andreas", category: "מתכונים", imgSrc: "img/cat-1.png"  },
		{ label: "antal1", category: "מתכונים", imgSrc: "img/cat-2.png"  },
		{ label: "antal2", category: "מתכונים", imgSrc: "img/cat-3.png"  },
		{ label: "antal3", category: "מתכונים", imgSrc: "img/cat-4.png"  },
		{ label: "annhhx10", category: "מדריכים", imgSrc: "img/cat-11.png"  },
		{ label: "annk K12", category: "מדריכים", imgSrc: "img/cat-6.png"  },
		{ label: "annttop C13", category: "מדריכים", imgSrc: "img/cat-7.png" },
		// { label: "anders andersson", category: "People" },
		// { label: "andreas andersson", category: "People" },
		// { label: "andreas johnson", category: "People" }
	];

	var placeholder = $(".js-search-results-placeholder");

	$("body")
	.on("mouseover", ".js-autocomplete-item", function() {
		var imgSrc = $(this).attr("cat-img");
		if (!imgSrc) return;

		placeholder.css({
			"background-image" : "url(" + imgSrc + ")" 
		});
	})
	.on("mouseout", ".js-autocomplete-item", function() {
		placeholder.css({
			"background-image" : "url()" 
		});
	});

	$( ".js-search-inner" ).catcomplete({
		delay: 0,
		source: data,
		appendTo: '.js-search-results'
	});	

	$( ".js-search-inner-popup" ).catcomplete({
		delay: 0,
		source: data,
		appendTo: '.js-search-results-popup'
	});	
} );


;(function($) {

	// $(".share__icon")
	// .on("mouseover", function() {
	// 	var svgID = $(this).find("use").attr("xlink:href");
	// 	$(svgID).addClass("hovered");
	// })
	// .on("mouseout", function() {
	// 	var svgID = $(this).find("use").attr("xlink:href");
	// 	$(svgID).removeClass("hovered");
	// });

})(jQuery);

//# sourceMappingURL=scripts.js.map